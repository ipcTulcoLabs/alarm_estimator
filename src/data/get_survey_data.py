import numpy as np
import pandas as pd


inpath = './data/external/'


def pull_survey(inpath):
    df = pd.read_excel(inpath+'pricing_data.xlsx',
                       sheet_name='Survey Data',
                       header=[0,1]).dropna(axis=1, how='all')
    df.columns = [' '.join(col).strip() for col in df.columns.values]
    df.columns = df.columns.str.replace(' ', '')
    df.rename(columns={'Unnamed:0_level_0Property': 'Property'}, inplace=True)
    return df


def pull_alarms(inpath):
    df = pd.read_excel(inpath + 'pricing_data.xlsx',
                       sheet_name='Footage Hour Details',
                       header=[2],
                       index_col=1).dropna(axis=1, how='all')
    df.columns = [' '.join(col).strip() for col in df.columns.values]
    df.columns = df.columns.str.replace(' ', '')
    return df

def make_survey_df(inpath):
    df = pull_survey(inpath)
